<?php

use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ControllerResolver;
use Symfony\Component\HttpKernel\Controller\ArgumentResolver;
use Symfony\Component\Routing\Loader\YamlFileLoader;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Router;


//use Symfony\Component\Config\Loader\FileLoader;
//use Symfony\Component\Routing\Route;
//use Symfony\Component\Routing\RouteCollection;
//use Symfony\Component\Routing\Loader\Configurator\RoutingConfigurator;

//use Dzion\App\Controllers\MainController;


$rootPath = ROOT_PATH;

$fileLocator = new FileLocator(array($rootPath));
$yamlLoader = new YamlFileLoader($fileLocator);
$routesPath = $rootPath . '/config/routes.yaml';
$options = ['cache_dir' => $rootPath . '/cache'];
$router = new Router($yamlLoader, $routesPath, $options);
$routesList = $router->getRouteCollection();

$request  = Request::createFromGlobals();
$pathInfo = $request->getPathInfo();
$context  = new RequestContext();
$context->fromRequest($request);

$pathInfo = getRootFolder($pathInfo, $rootPath);

$matcher = new UrlMatcher($routesList, $context);
$attributes = $matcher->match($pathInfo);
$request->attributes->add($attributes);

$controllerResolver = new ControllerResolver();
$controller = $controllerResolver->getController($request);

$argResolver = new ArgumentResolver();
$arguments = $argResolver->getArguments($request, $controller);

$response = call_user_func_array($controller, $arguments);

// lg([$response, $arguments, $controller]);



//$routes = new RoutingConfigurator();
//lg($routes);
//
//$routes->add('hello_page', '/hello')
//       ->controller([MainController::class, 'hello']);


//$routes = new RouteCollection();
//
//$routes->add('main',
//    new Route('/', ['_controller' => MainController::class, '_action' => 'index', '_type' => 'html'])
//);
//
//$routes->add('hello',
//    new Route('/hello', ['_controller' => 'Dzion\App\Controllers\MainController::hello', '_action' => 'hello', '_type' => 'html'])
//);
//
//
//$routes->add('test_name',
//    new Route('/test-name/{userName}/{role}',
//                   ['_controller' => MainController::class, '_action' => 'testName', '_type' => 'json'])
//);

// return $routes;