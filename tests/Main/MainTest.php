<?php

use PHPUnit\Framework\TestCase;

class MainTest extends TestCase
{
    protected $main;

    protected function setUp() : void {
        define('ROOT_PATH', dirname(__DIR__));
        $this->main = new \Dzion\App\Controllers\MainController();
    }

    protected function tearDown() : void {
        $this->main = NULL;
    }

    public function testMultiplay() : void
    {
        $res = $this->main->multiplay(2, 4);
        $this->assertEquals(6, $res);
    }

}