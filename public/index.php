<?php

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");

define('ROOT_PATH'   , dirname(__DIR__));
require_once ROOT_PATH . '/src/Utils/bootstrap.php';
require_once ROOT_PATH . '/vendor/autoload.php';

error_reporting(E_ALL); // включаем обработку всех ошибок
set_error_handler("customErrorWarningHandler"); // обработка предупреждений
register_shutdown_function('customFatalErrorHanler'); // обработка всех ошибок в том числе и фатальных

use Dzion\System\AppKernel;

try {
    $app = new AppKernel(ROOT_PATH, ROUTES_PATH);
    $response = $app->handle();
} catch (ErrorException $err) {
    $errorMessage = $err->getMessage();
    logFileSave($errorMessage, FATAL_ERROR_LOG);
}

$response->send();






