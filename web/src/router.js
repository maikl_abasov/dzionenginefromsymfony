
import {createRouter, createWebHashHistory} from "vue-router";

import MainPage    from './pages/Main';
import CarsSearch    from './pages/CarsSearch';
import UserProfile from './pages/UserProfile';
import TestPage    from './pages/TestPage';
import ErrorPage   from './pages/ErrorPage';

export default createRouter({
    history:createWebHashHistory(),
    routes : [
        { path: '/main'        , component: MainPage, alias: '/' },
        { path: '/table-info'  , component: CarsSearch },
        { path: '/user-profile', component: UserProfile },
        { path: '/test-page'   , component: TestPage },
        { path: '/error-page'  , component: ErrorPage },
    ]
})