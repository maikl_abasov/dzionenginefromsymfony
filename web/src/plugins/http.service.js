import axios from "axios";

const API_URL = 'http://dzion.ru';
const RESULT_NAME = 'result';

const HttpService = {
    data() {
        return {
           apiUrl : API_URL,
           resultName : RESULT_NAME,
           fatalError : {},
           debug    : [],
           error    : [],
           httpInfo : [],

        }
    },
    methods: {

         getResponseData(response) {
            return response[this.resultName];
         },

         httpGet(url, callback = null) {
             const apiUrl = this.apiUrl + url;
             const headers = { 'Content-Type': 'application/json' };
             axios({ method: 'get',
                     url: apiUrl,
                     headers,
                     withCredentials: false,
             }).then( (response) => {
                 //console.log(response);
                 let data = response.data;
                 this.debugHandler(data);
                 if(callback) callback(data);
                 return data;
             }).catch(this.fatalErrorHandler);
         },

         httpPost(url, postData = {}, callback = null) {
             const apiUrl = this.apiUrl + url;
             const headers = { 'Content-Type': 'application/json' };
             axios({ method: 'post',
                    url: apiUrl,
                    headers,
                    withCredentials: false,
                    data: postData
             }).then( (response) => {
                    //console.log(response);
                    let data = this.getResponseData(response);
                    this.debugHandler(data);
                    if(callback) callback(data);
                    return data;
             }).catch(this.fatalErrorHandler);
         },

         fatalErrorHandler(error) {
             let response = (error.response) ? error.response : {};
             let info = {
                 code: error.code,
                 message: error.message,
                 data: response.data,
                 status: response.status,
                 text: response.statusText,
             }
             this.fatalError = info;
             console.log('HTTP ERROR!!!');
             console.log('ERROR-INFO:', info);
             console.log(error);
         },

         debugHandler(data) {
             if(data['debug']) this.debug = data['debug'];
             if(data['error']) this.error = data['error'];
         },

    }
}

export default HttpService;


// class HttpService {
//
//     constructor() {
//         let service = axios.create({
//             baseURL: ApiUrl,
//             headers: { csrf: 'token' }
//         });
//         service.interceptors.response.use(this.handleSuccess, this.handleError);
//         this.service = service;
//     }
//
//     handleSuccess(response) {
//         return response;
//     }
//
//     handleError = (error) => {
//         switch (error.response.status) {
//             case 401: this.redirectTo(document, '/error-page'); break;
//             case 404: this.redirectTo(document, '/error-page'); break;
//             default: this.redirectTo(document, '/error-page') ; break;
//         }
//         return Promise.reject(error)
//     }
//
//     redirectTo = (document, path) => {
//         document.location = path
//     }
//
//     get(path, callback) {
//         console.log(path);
//         return this.service.get(path).then(
//             (response) => callback(response.status, response.data)
//         );
//     }
//
//     patch(path, payload, callback) {
//         return this.service.request({
//             method: 'PATCH',
//             url: path,
//             responseType: 'json',
//             data: payload
//         }).then((response) => callback(response.status, response.data));
//     }
//
//     post(path, payload, callback) {
//         return this.service.request({
//             method: 'POST',
//             url: path,
//             responseType: 'json',
//             data: payload
//         }).then((response) => callback(response.status, response.data));
//     }
// }
//
// export default new HttpService;

// export default {
//     http : axios.create({
//         baseURL: ApiUrl,
//         headers: {
//             Authorization: 'Bearer {toke}',
//         }
//     })
// }