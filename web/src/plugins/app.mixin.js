const AppMixin = {
    data() {
        return {}
    },

    methods: {

        htmlElemsRender(selector, func) {
            let elements = document.querySelectorAll(selector)
            elements.forEach(func);
        },

        elemStyleUpdate(selector, fn) {
            let elements = document.querySelectorAll(selector)
            elements.forEach(fn);
        },

        lg(arr) {

            var result = ' ---> Нет результатов <---';
            if (arr) result = _printf(arr); // --- формируем строку из массива
            _openWindow(result);            // --- показываем результат в новом окне

            // --- формируем строку из массива
            function _printf(arr) {

                if (typeof (arr) !== 'object')
                    return '<ul><li>' + arr + '</li></ul>';

                var strResult = '';
                var deLimiter = ' => ';

                for (var i in arr) {
                    var values = arr[i];
                    var subValues = '';
                    var li = deLimiter;
                    if (typeof (values) == 'object') {
                        subValues = _printf(values);
                    } else {
                        li += values;
                    }
                    strResult += '<li>[' + i + ']' + li + '</li>' + subValues;
                }

                return '<ul>' + strResult + '</ul>';
            }

            // --- показываем результат в новом окне
            function _openWindow(result, href) {
                var modal = window.open('', '', 'scrollbars=1');
                var style = 'button { padding:10px; margin:10px; border:0px grey solid; width:30%; cursor:ponter  }'
                    + ' .lg-view-result { border:2px red solid; }';
                var html = '<!DOCTYPE html><head><style>' + style + '</style><head>'
                    + '<p><button onclick="window.close();" >Close</button></p><hr>'
                    + '<p class="lg-view-result" ><pre>' + result + '</pre></p>';
                modal.document.body.innerHTML = html;
            }
        }

    }
}

export default AppMixin;

