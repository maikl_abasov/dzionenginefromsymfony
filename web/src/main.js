import { createApp } from 'vue'
import App from './App.vue'
import Router from './router'
import HttpService from './plugins/http.service'
import AppMixin from './plugins/app.mixin'
import Store from './store'
import './assets/tailwind.css'

const VueApp = createApp(App)
VueApp.use(Store);
VueApp.use(Router);
VueApp.mixin(AppMixin);
VueApp.mixin(HttpService);
VueApp.mount('#app');