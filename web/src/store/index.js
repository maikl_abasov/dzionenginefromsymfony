import {createStore} from 'vuex'
import axios from "axios";

const API_URL = 'http://dzion.ru';
const RESULT_NAME = 'result';

export default createStore({

    state: {
        itemsCount : 0,
        filters : {
            like    : {},
            only    : {},
            order_by: {},
            search  : '',
            paginate : { page: 1 , limit :10 }
        },
        carsList: [],
        clearFlag: false,

        httpInfo : {},
        preloaderStatus: false,
        preloaderText: '',

    },

    mutations: {

        setHttpInfo(state, payload) {
            state.httpInfo[payload.fname] = payload.data;
        },

        setItemsCount(state, value) {
            state.itemsCount = value
        },

        setPageLimit(state, value) {
            state.filters.paginate.limit = value
        },

        setPageNum(state, value) {
            state.filters.paginate.page = value
        },

        setCarsList(state, data) {
            state.carsList = data
        },

        changePreloader(state, data) {
           state.preloaderStatus = data.status;
           state.preloaderText  = data.text;
        },

        // Устанавливаем новые значения фильтра
        setFilters(state, payload) {
            let type  = payload.type;
            let field = payload.field;
            let data  = payload.data;

            if(!data) {
                if(field) delete state.filters[type][field]
                else      state.filters[type] = '';
                return false;
            }

            if(field) state.filters[type][field] = data;
            else      state.filters[type] = data;
        },

        // Очищаем значения фильтра
        clearFilters(state, payload = null) {
            let filters = {
                search  : '',
                like    : {},
                only    : {},
                order_by: {},
                paginate : { page: 1 , limit :30 }
            };
            state.filters = filters;
            state.clearFlag = true;
        },
    },

    actions: {

        // Выборка данных
        async fetchFiltersData({commit, state}) {

            commit("changePreloader", { status: true, text: '' });

            let filters = state.filters;
            let url     = API_URL + '/cars/table-info-filters';
            axios({
                method  : 'post', url, withCredentials: false,
                headers : { 'Content-Type': 'application/json' },
                data : filters
            }).then( (response) => {

                console.log(response);
                let resp  = getResponseData(response);
                let debug = debugHandler(resp);

                let data   = resp[RESULT_NAME];
                let counts = resp.counts;
                commit('setHttpInfo', { fname: 'debug', data: debug });
                commit("setCarsList", data);

                if(counts)
                    commit("setItemsCount", counts);

                commit("changePreloader", { status: false, text: '' });

            }).catch(error => {
                let errorInfo = fatalErrorHandler(error)
                commit('setHttpInfo', { fname: 'error', data: errorInfo});
                commit("changePreloader", { status: false, text: '' });
            });

            function fatalErrorHandler(error) {
                let response = (error.response) ? error.response : {};
                let info = {
                    code: error.code,
                    message: error.message,
                    data: response.data,
                    status: response.status,
                    text: response.statusText,
                }

                console.log('HTTP ERROR!!!');
                console.log('ERROR-INFO:', info);
                console.log(error);
                return info;
            }

            function debugHandler(data) {
                let info  = {}
                if(data['debug']) info['debug'] = data['debug'];
                if(data['error']) info['error'] = data['error'];
                return info;
            }

            function getResponseData(response) {
                return response.data;
            }
        },

    },

    getters: {

        getItemsCount: (state) => {
            return state.itemsCount;
        },

        getPageLimit: (state) => {
            return state.filters.paginate.limit;
        },

        getPageNum: (state) => {
            return state.filters.paginate.page;
        },

        getCarsList: (state) => {
            return state.carsList
        },

        getPagesCount: (state) => {
            let paginate = state.filters.paginate;
            return Math.ceil(state.itemsCount / paginate.limit)
        },
    },

});
