<?php

namespace Dzion\App\Controllers;

use Symfony\Component\HttpFoundation\JsonResponse;
use Dzion\System\BaseController;
use Dzion\App\Models\User;


class MainController extends BaseController {

    protected $userModel;

    public function __construct(){
        parent::__construct();
        $this->userModel = new User();
    }

    public function index() : JsonResponse {
        $message = '::index';
        // $this->userModel->createTable();
        return $this->dataResponse($message);
    }

    public function hello() : JsonResponse {
        $message = '::hello';
        return $this->dataResponse($message);
    }

    public function testName(string $userName, string $role) : JsonResponse {
        $response =  ['name1' => $userName, 'role1' => $role];
        return $this->jsonResponse($response);
    }

    public function multiplay(int $a, int $b) : int {
        $res =  $a + $b;
        return $res;
    }

}