<?php

namespace Dzion\App\Controllers;

use Dzion\App\Models\Car;
use Symfony\Component\HttpFoundation\JsonResponse;
use Dzion\System\BaseController;
use Symfony\Component\HttpFoundation\Request;


class CarController extends BaseController {

    protected $car;
    protected $filtersList = [];

    public function __construct(){
        parent::__construct();
        $this->car = new Car();
    }

    // Первичная выборка
    public function getTableInfo(string $rowsLimit = '30') : JsonResponse {
        $data = $this->car->getCars([], $rowsLimit);
        $carsCount = $this->car->getCarsDataCounts();
        return $this->jsonResponse($data, ['counts' => $carsCount]);
    }

    // Выборка по фильтрам
    public function getTableInfoFiltered() {
        $filters = $this->retriveJsonData();
        $this->filtersList = $filters;
        $resp = $this->car->carFiltersRun($filters);
        $data   = $resp['result'];
        $counts = $resp['counts'];
        return $this->jsonResponse($data, ['counts' => $counts]);
    }

    // Поиск по данным для формирования выпадающих фильтров
    public function getFieldOnlyFilterItem(string $fname, string $searchValue) {
        $limit = 500;
        $searchValue = urldecode($searchValue);
        $data = $this->car->getFieldOnlyFilterItem($fname, $searchValue, $limit);
        return $this->jsonResponse($data);
    }

}