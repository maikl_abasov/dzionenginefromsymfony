<?php

namespace Dzion\App\Models;

use Dzion\System\BaseModel;
use Dzion\System\CustomPdoExection;

class Car extends BaseModel
{
    protected $table = 'cbn_lots';

    //
    public function getCarsDataCounts($where = []) {
        $table = $this->table;

        $partyFields = $this->getThirdPartyFields();
        $fieldsList[] = "{$table}.*";
        foreach ($partyFields as $fname => $newName) {
            $fieldsList[] = "{$fname} AS {$newName}";
        }

        $data = $this->query()
            ->select(...$fieldsList)
            ->leftJoin('geo__cities        AS geo',   "{$table}.city", '=', 'geo.city_id')
            ->leftJoin('avc_marks          AS mark',  "{$table}.mark", '=', 'mark.id')
            ->leftJoin('avc_models         AS model', "{$table}.model", '=', 'model.id')
            ->leftJoin('avc_generations    AS gen',   "{$table}.generation", '=', 'gen.id')
            ->leftJoin('avc_modifications  AS modif',   "{$table}.modification", '=', 'modif.id')
            ->leftJoin('cbn_profiles       AS pr',    "{$table}.acc_id", '=', 'pr.id')
            ->leftJoin('cbn_avito_data     AS avito', "{$table}.id", '=', 'avito.lot_id')
            ->where($where);

        $result = $data->count();
        return $result;
    }

    // Первичная выборка
    public function getCars( $where = [], $limit = 0) {
        $table = $this->table;

        $partyFields = $this->getThirdPartyFields();
        $fieldsList[] = "{$table}.*";
        foreach ($partyFields as $fname => $newName) {
            $fieldsList[] = "{$fname} AS {$newName}";
        }

        $data = $this->query()
            ->select(...$fieldsList)
            ->leftJoin('geo__cities        AS geo',   "{$table}.city", '=', 'geo.city_id')
            ->leftJoin('avc_marks          AS mark',  "{$table}.mark", '=', 'mark.id')
            ->leftJoin('avc_models         AS model', "{$table}.model", '=', 'model.id')
            ->leftJoin('avc_generations    AS gen',   "{$table}.generation", '=', 'gen.id')
            ->leftJoin('avc_modifications  AS modif',   "{$table}.modification", '=', 'modif.id')
            ->leftJoin('cbn_profiles       AS pr',    "{$table}.acc_id", '=', 'pr.id')
            ->leftJoin('cbn_avito_data     AS avito', "{$table}.id", '=', 'avito.lot_id')
            ->where($where);

        if($limit)
            $data->limit($limit);
        $result = $data->get();
        return $result;
    }

    // Выборка по фильтрам
    public function carFiltersRun($filters = []) {

        $whereBlocks =  $inOnlyArr = $oderByArr = [];
        $table = $this->table;

        $whereLike   = (isset($filters['like']))      ? (array)$filters['like'] : [];
        $whereInOnly = (isset($filters['only']))      ? (array)$filters['only'] : [];
        $orderBy     = (isset($filters['order_by']))  ? (array)$filters['order_by'] : [];
        $paginate    = (isset($filters['paginate']))  ? (array)$filters['paginate'] : ['page' => 1, 'limit' => 30];
        $searchValue = (isset($filters['search']))    ? $filters['search'] : '';

        //--- Формируем имена полей для выборки
        $partyFields = $this->getThirdPartyFields();
        $fieldsList[] = "{$table}.*";
        foreach ($partyFields as $fname => $newName) {
            $fieldsList[] = "{$fname} AS {$newName}";
        }
        $selectFieldNames = implode(',', $fieldsList);

        /////// Фильтр 1 //////////
        //--- Поиск по всей таблице [seacrh]
        if($searchValue) {
            $searchFields = $this->setOrSearchFields($searchValue);
            if(!empty($searchFields))
                $whereBlocks['search'] = ' (' .implode(" OR ", $searchFields) . ') ';

        }

        /////// Фильтр 2 //////////
        // Поиск по отдельному полю таблицы [like]
        if(!empty($whereLike)) {
            $res = [];
            foreach ($whereLike as $fName => $value) {
                $key   = array_search($fName, $partyFields);
                $fName = ($key != false) ? $key : $table .'.'. $fName;
                $res[] = "{$fName} LIKE '%{$value}%'";
            }
            if(!empty($res))
                $whereBlocks['like'] = '(' . implode(" AND ", $res) . ')';

        }

        /////// Фильтр 3 //////////
        // Поиск по фильтрам [only]
        foreach ($whereInOnly as $fName => $items) {
            if(empty($items)) continue;
            $fKey   = array_search($fName, $partyFields);
            $fName = ($fKey != false) ? $fKey : $table .'.'. $fName;
            foreach ($items as $key => $value) {
                $items[$key] = "'$value'";
            }
            $lines = implode(",", $items);
            $inOnlyArr[] = "{$fName} IN ({$lines})";
        }

        if(!empty($inOnlyArr)) {
            $whereBlocks['only'] = '(' .implode(" OR ", $inOnlyArr) . ')';
        }

        /////// Сортировка //////////
        // Сортировка [order_by]
        foreach ($orderBy as $fName => $type) {
            if(!$type) continue;
            $fKey   = array_search($fName, $partyFields);
            $fName = ($fKey != false) ? $fKey : $table .'.'. $fName;
            $oderByArr[] = "ORDER BY {$fName} {$type}";
        }

        $orderBySql = '';
        if(!empty($oderByArr)) {
            $orderBySql = implode(",", $oderByArr);
        }

        ////////////////////////////
        // Формируем условия выборки
        $whereCondition = '';
        if(!empty($whereBlocks)) {
            $condition = implode(" AND ", $whereBlocks);
            $whereCondition = " WHERE {$condition} ";
        }

        // Формируем пагинацию
        $page = $paginate['page'];
        $limit = $paginate['limit'];
        $pageLine = ($page * $limit) - $limit;
        $paginateLine = " LIMIT {$pageLine}, {$limit}";

        $fromJoinSql = "
           FROM {$table}
           LEFT JOIN geo__cities       AS geo   ON {$table}.city = geo.city_id
           LEFT JOIN avc_marks         AS mark  ON {$table}.mark = mark.id
           LEFT JOIN avc_models        AS model ON {$table}.model = model.id 
           LEFT JOIN avc_generations   AS gen   ON {$table}.generation = gen.id 
           LEFT JOIN avc_modifications AS modif   ON {$table}.modification = modif.id 
           LEFT JOIN cbn_profiles      AS pr    ON {$table}.acc_id = pr.id 
           LEFT JOIN cbn_avito_data    AS avito ON {$table}.id = avito.lot_id 
           {$whereCondition}
        ";

        $query = "
           SELECT  {$selectFieldNames}
           {$fromJoinSql}
           {$orderBySql}
           {$paginateLine} ";

        $queryCounts = "SELECT COUNT(*) {$fromJoinSql} ";

        // lg($query);

        $db = $this->getDbFacade(); // Получаем DB Facade
        $result = $db->select($query); // Выполняем запрос
        $counts = $db->select($queryCounts); // Получаем количество
        if(isset($counts[0])) {
            $counts = (array)$counts[0];
            $counts = $counts['COUNT(*)'];
        }
        return ['counts' => $counts, 'result'=> $result];

    }

    // Поиск по данным для формирования выпадающих фильтров
    public function getFieldOnlyFilterItem($fname, $searchValue, $limit = 0) {

        $partyFields = $this->getThirdPartyFields();
        $joinField = array_search($fname, $partyFields);
        $whereCondition = $where = $groupBy = '';
        $table = $this->table;

        if($joinField != false)  {
            $selectName = "{$joinField} AS {$fname}";
            $where = " {$joinField} LIKE '%{$searchValue}%'";
            $groupBy = " GROUP BY {$joinField}";
        } else {
            $selectName = "{$table}.{$fname}";
            $where = " {$table}.{$fname} LIKE '%{$searchValue}%'";
            $groupBy = " GROUP BY {$table}.{$fname}";
        }

        if($searchValue) {
            $whereCondition = " WHERE {$where} ";
        }

        $limit = ($limit) ? ' LIMIT ' . $limit : '';  // Формируем лимит

        $query = "
           SELECT 
              {$selectName}
           FROM {$table}
           LEFT JOIN geo__cities       AS geo   ON {$table}.city = geo.city_id
           LEFT JOIN avc_marks         AS mark  ON {$table}.mark = mark.id
           LEFT JOIN avc_models        AS model ON {$table}.model = model.id 
           LEFT JOIN avc_generations   AS gen   ON {$table}.generation = gen.id 
           LEFT JOIN avc_modifications AS modif   ON {$table}.modification = modif.id 
           LEFT JOIN cbn_profiles      AS pr    ON {$table}.acc_id = pr.id 
           LEFT JOIN cbn_avito_data    AS avito ON {$table}.id = avito.lot_id 
           
           {$whereCondition}
           {$groupBy}
           {$limit} ";

        $db = $this->getDbFacade(); // Получаем DB Facade
        $response = $db->select($query); // Выполняем запрос
        return $response;
    }

    // Поля связанных таблиц
    public function getThirdPartyFields() {
        return [
            "geo.city"       => "city_name",
            "mark.name"      => "mark_name",
            "model.name"     => "model_name",
            "gen.name"       => "gen_name",
            "modif.name"     => "mod_name",
            "pr.cmpForm"     => "cmpForm",
            "pr.cmpName"     => "cmpName",
            "avito.raw_data" => "avtoteka",
        ];
    }

    // Список полей для поиска по всей таблице
    protected function setOrSearchFields(string $searchValue) : array {

        $table = $this->table;
        $fields =  [
            "id"         => $table. '.id',
            "acc_id"     => $table. '.acc_id',
            "vin"        => $table. '.vin',
            "city_name"  => 'geo.city',
            "mark_name"  => 'mark.name',
            "model_name" => 'model.name',
            "gen_name"   => 'gen.name',
            "mod_name"   => 'modif.name',
            "cmpName"    => 'pr.cmpName',
        ];

        $searchSql = [];
        foreach ($fields as $fAlias => $fName) {
            $searchSql[] = "{$fName} LIKE '%$searchValue%'";
        }

        return $searchSql;
    }

    protected function testData() {
        return array(
            'like'     => ['acc_id' => 6, ],
            'only'     => ['city' => [ 0 => 780, 1 => 2287], ],
            'order_by' => ['city' => 'DESC',],
            'search'   => 'Au',
        );
    }

}
