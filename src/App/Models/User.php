<?php

namespace Dzion\App\Models;

use Dzion\System\BaseModel;

class User extends BaseModel
{
    protected $table = 'users';
    protected $fillable = ['username', 'email', 'password'];

    public function createTable() {

        $this->capsule->schema()->create('users', function ($table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('userimage')->nullable();
            $table->string('api_key')->nullable()->unique();
            $table->rememberToken();
            $table->timestamps();
        });

//        $query = "
//            CREATE TABLE `users` (
//                 `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
//                 `username` varchar(100) DEFAULT NULL,
//                 `email` varchar(200) DEFAULT NULL,
//                 `password` varchar(200) DEFAULT NULL,
//                 `created_at` timestamp NULL DEFAULT NULL,
//                 `updated_at` timestamp NULL DEFAULT NULL,
//                 `deleted_at` timestamp NULL DEFAULT NULL,
//             PRIMARY KEY (`id`)
//            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
//         ";

    }
}