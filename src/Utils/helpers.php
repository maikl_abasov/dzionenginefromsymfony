<?php

function lg($data) {
    $output = print_r($data, true);
    echo "<pre>$output</pre>";
    die();
}

function debug($data, $fname = 'data') {
    die(json_encode([$fname => $data]));
}

function getRootFolder($pathInfo, $rootPath) {

    $PHP_SELF = explode("/", trim($_SERVER['PHP_SELF'], "/"));
    $pathInfoArr = explode("/", trim($pathInfo, "/"));

    foreach ($PHP_SELF as $folderName) {
        foreach ($pathInfoArr as $urlKey => $urlName) {
            if($folderName != $urlName) continue;
            unset($pathInfoArr[$urlKey]);
        }
    }

    $pathInfo = "/" . implode("/", $pathInfoArr);
    return $pathInfo;
}


function logFileSave($logData, $logFileName = INFO_LOG){
   $logFilePath = ROOT_PATH . LOGS_DIR . '/' . $logFileName;
   $data = $logData;
   if(file_exists($logFilePath)) {
       $current = file_get_contents($logFilePath);
       $data = $logData . $current;
   }
   $save = file_put_contents($logFilePath, $data,  LOCK_EX);
   return $save;
}

//--- Функция обработки ошибок
function customErrorWarningHandler($errNo, $errMessage, $errFile, $errLine) {

    // может потребоваться экранирование $errstr:
    $errMessage = htmlspecialchars($errMessage);
    $requestUri = $_SERVER['REQUEST_URI'];

    $today = date('Y.m.d__H:i:s');
    $errorLog  = "Date : {$today} \n";
    $errorLog .= "Type : PHP ERROR \n";
    $errorLog .= "RequestUri : {$requestUri} \n";
    $errorLog .= "Title: Обработчик ошибок (customErrorWarningHandler) \n";
    $errorLog .= "Place: line={$errLine}; file={$errFile}\n";
    $errorLog .= "ErrNo: {$errNo} \n";
    $errorLog .= "Message :  {$errMessage}\n";
    $errorLog .= "\n";

    logFileSave($errorLog, WARNING_ERROR_LOG);

}

function customFatalErrorHanler() {

    $error = error_get_last();

    if (is_array($error) &&  in_array($error['type'], [E_ERROR, E_PARSE, E_CORE_ERROR, E_COMPILE_ERROR])) {
        while (ob_get_level()) {
            ob_end_clean();
        }

        $errorTypes = array (
            E_ERROR              => 'Фатальная ошибка',
            E_WARNING            => 'Предупреждение',
            E_PARSE              => 'Ошибка разбора исходного кода',
            E_NOTICE             => 'Уведомление',
            E_CORE_ERROR         => 'Ошибка ядра',
            E_CORE_WARNING       => 'Предупреждение ядра',
            E_COMPILE_ERROR      => 'Ошибка на этапе компиляции',
            E_COMPILE_WARNING    => 'Предупреждение на этапе компиляции',
            E_USER_ERROR         => 'Пользовательская ошибка',
            E_USER_WARNING       => 'Пользовательское предупреждение',
            E_USER_NOTICE        => 'Пользовательское уведомление',
            E_STRICT             => 'Уведомление времени выполнения',
            E_RECOVERABLE_ERROR  => 'Отлавливаемая фатальная ошибка'
        );

        $errNo      = $error['type'];
        $errMessage = $error['message'];
        $errFile    = $error['file'];
        $errLine    = $error['line'];
        $errConst   = $errorTypes[$errNo];
        $requestUri = $_SERVER['REQUEST_URI'];

        $today = date('Y.m.d__H:i:s');
        $errorLog  = "Date : {$today} \n";
        $errorLog .= "Type : PHP ERROR \n";
        $errorLog .= "RequestUri : {$requestUri} \n";
        $errorLog .= "Title: Обработчик ошибок (allErrorHanler) \n";
        $errorLog .= "Place: line={$errLine}; file={$errFile}\n";
        $errorLog .= "ErrNo: {$errNo} \n";
        $errorLog .= "ErrConst: {$errConst} \n";
        $errorLog .= "Message :  {$errMessage}\n";
        $errorLog .= "\n";

        logFileSave($errorLog, FATAL_ERROR_LOG);
    }
}
