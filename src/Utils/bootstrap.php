<?php

define('SYSTEM_CONTROL', 'start');
define('CORE_PATH'   , ROOT_PATH . '/src/Core');
define('APP_PATH'    , ROOT_PATH . '/src/App');
define('ROUTES_PATH' , ROOT_PATH . '/config/routes.yaml');

define('LOGS_DIR'          , '/logs');
define('FATAL_ERROR_LOG'   , 'fatal_errors.log');
define('WARNING_ERROR_LOG' , 'warning_errors.log');
define('INFO_LOG'          , 'log_info.log');
