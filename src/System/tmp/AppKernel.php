<?php

namespace Dzion\System;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Controller\ControllerResolver;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Loader\YamlFileLoader;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\EventDispatcher\EventDispatcher;


class AppKernel {

    protected $routes;
    protected $request;
    protected $response;
    protected $eventDispatcher;
    protected $ROOT_PATH;

    public function __construct(string $rootPath, RouteCollection $routes){

        $this->ROOT_PATH = $rootPath;
        $this->request  = Request::createFromGlobals();
        $this->response = new Response();
        $this->eventDispatcher = new EventDispatcher();
        $this->routes = $routes;

    }

    public function handle(){

        $request = $this->request;
        $routes  = $this->routes;

        // создаем контекст, используя данные запроса
        $context = new RequestContext();
        $context->fromRequest($request);
        $matcher = new UrlMatcher($routes, $context);

        try {

            $pathInfo = $request->getPathInfo();
            $rootPath = $this->getRootPath();
            $pathInfo = $this->getRootFolder($pathInfo, $rootPath);

            $attributes = $matcher->match($pathInfo);
            $request->attributes->add($attributes);

            $controllerResolver = new ControllerResolver();
            $controller = $controllerResolver->getController($request);
            lg($controller);

//            $controller = $attributes['_controller'];
//            $action     = $attributes['_action'];
//            $respType   = $attributes['_type'];
//
//            $refClass = new \ReflectionClass($controller);
//            $parameters = $refClass->getMethod($action)->getParameters();
//            $params = array();
//            foreach($parameters as $param) {
//                $name =  $param->name;
//                if(!empty($attributes[$name])) {
//                    $params[] = $attributes[$name];
//                }
//            }
//
//            $class = new $controller($request);
//            $response = $class->$action(...$params);

        } catch (ResourceNotFoundException $err) {
            $response = new Response('Не найден маршрут!', Response::HTTP_NOT_FOUND);
        }

        switch($respType) {
            case 'json' : $this->setJsonResponse($response); break;
            default     : $this->setResponse($response);     break;
        }

        return $this->getResponse();
    }

    protected function getRootPath() {
        return $this->ROOT_PATH;
    }

    protected function setJsonResponse($data) {
        $this->response->setContent(json_encode($data));
        $this->response->headers->set('Content-Type', 'application/json');
    }

    protected function setResponse(string $data) {
        $this->response->setContent($data);
    }

    protected function getResponse() {
        return $this->response;
    }

    protected function loadYamlFile($filePath){
        $rootPath = $this->getRootPath();
        $fileLocator = new FileLocator(array($rootPath));
        $loader = new YamlFileLoader($fileLocator);
        $file = $loader->load($filePath);
        return $file;
    }

    protected function getRootFolder($pathInfo, $rootPath) {

        $PHP_SELF = explode("/", trim($_SERVER['PHP_SELF'], "/"));
        $pathInfoArr = explode("/", trim($pathInfo, "/"));

        foreach ($PHP_SELF as $folderName) {
            foreach ($pathInfoArr as $urlKey => $urlName) {
                if($folderName != $urlName) continue;
                unset($pathInfoArr[$urlKey]);
            }
        }

        $pathInfo = "/" . implode("/", $pathInfoArr);
        return $pathInfo;
    }

    public function on($event, $callback){
        $this->eventDispatcher->addListener($event, $callback);
    }

    public function fire($event){
        return $this->eventDispatcher->dispatch($event);
    }

}