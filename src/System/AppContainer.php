<?php

namespace Dzion\System;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Path;

class AppContainer
{
    public $container;

    public function __construct(){
        $this->container = new ContainerBuilder();
        $this->init();
    }

    protected function init() : void {
        $this->container->register('filesystem', Filesystem::class);
    }

    public function getContainer() : ContainerBuilder {
        return $this->container;
    }

    public function get(string $key) {
        return $this->container->get($key);
    }

}