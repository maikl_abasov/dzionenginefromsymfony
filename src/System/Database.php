<?php

namespace Dzion\System;

use Symfony\Component\Dotenv\Dotenv;
use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Events\Dispatcher;
use Illuminate\Container\Container;

class Database
{
    protected $config;
    protected $capsule;

    public function __construct() {
        $this->config = $this->setConfig();
        $this->connection($this->config);
    }

    public function getCapsule() {
        return $this->capsule;
    }

    protected function connection($config) {
        try {
            $this->capsule = new Capsule();
            $this->capsule->addConnection($config);
            $this->capsule->setEventDispatcher(new Dispatcher(new Container));
            $this->capsule->setAsGlobal();
            $this->capsule->bootEloquent();
        } catch(\PDOException $exception) {
            throw new \Exception('Ошибка подключения к базе');
        }

    }

    protected function setConfig() {
        $config = [
            'driver'    => $this->getEnv('DB_DRIVER'),
            'host'      => $this->getEnv('DB_HOST'),
            'database'  => $this->getEnv('DB_BASE'),
            'username'  => $this->getEnv('DB_USER'),
            'password'  => $this->getEnv('DB_PASSWORD'),
            'charset'   => $this->getEnv('DB_CHARSET'),
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
        ];
        return $config;
    }

    protected function getEnv($fName) {
        return $_ENV[$fName];
    }

    public function createDbFacade() {

        $app = new Container();
        \Illuminate\Support\Facades\Facade::setFacadeApplication($app);

        $app->singleton('db', function () use ($app) {
            $capsule = new \Illuminate\Database\Capsule\Manager;
            $config = $this->setConfig();
            $capsule->addConnection($config);
            $capsule->setAsGlobal();
            $capsule->bootEloquent();
            return $capsule;
        });

        $app->singleton('hash', function () use ($app) {
            return new \Illuminate\Hashing\HashManager($app);
        });

        class_alias(\Illuminate\Support\Facades\DB::class, 'DB');
        class_alias(\Illuminate\Support\Facades\Hash::class, 'Hash');
    }

}