<?php

namespace Dzion\System;

use Illuminate\Database\Eloquent\Model;
use Dzion\System\Database;
use Illuminate\Support\Facades\DB;

abstract class BaseModel extends Model
{
    protected $db;
    protected $capsule;

    public function __construct(){
       $this->db = new Database();
       $this->capsule = $this->db->getCapsule();
    }

    protected function getDbFacade() {
        $this->db->createDbFacade();
        return DB::connection();
    }

    protected function getItems($tableName, $fields = ["*"], $where = [], $limit = 0) {
        $data = $this->capsule->table($tableName)
              ->select(...$fields)
              ->where($where);

        if($limit)
            $data->limit($limit);
        $result = $data->get();
        return $result;
    }

    public function getFromTableData($fname) {

        $tableName = '';
        switch($fname) {
            case  'city_name' :
                $tableName = 'geo__cities';
                $where = ['city_id', 'city'];
                break;

            case  'mark_name' :
                $tableName = 'avc_marks';
                $where = ['id', 'name'];
                break;

            case  'model_name' :
                $tableName = 'avc_models';
                $where = ['id', 'name'];
                break;

            case  'gen_name' :
                $tableName = 'avc_generations';
                $where = ['id', 'name'];
                break;

            case  'mod_name' :
                $tableName = 'avc_modifications';
                $where = ['id', 'name'];
                break;
        }

        $reference = ($tableName) ? $this->getItems($tableName, $where) : [];
        return $reference;
    }

    protected function errorFormat(\PDOException $err, $infoArr = [], $formatType = 'string') {

        $errorInfo = $err->errorInfo;
        $today = date('Y.m.d__H:i:s');
        $response = false;
        $errTitle = (isset($infoArr['title'])) ? $infoArr['title'] : '';
        $errPlace = (isset($infoArr['place'])) ? $infoArr['place'] : '';

        switch ($formatType) {
            case 'array' :
                $response = [
                    'type'  => 'PDO',
                    'date'  => $today,
                    'title' => $errTitle,
                    'info'  => $errorInfo,
                    'place' => $errPlace,
                ];
                break;

            case 'string' :
                $response = $this->errorTextFormat($errorInfo, $errTitle, $errPlace);
                break;
        }
        return $response;
    }

    protected function errorTextFormat($errorInfo, $errTitle, $errPlace) {

        $today = date('Y.m.d__H:i:s');
        $type    = (isset($errorInfo[0])) ? $errorInfo[0] : "";
        $code    = (isset($errorInfo[1])) ? $errorInfo[1] : "";
        $message = (isset($errorInfo[2])) ? $errorInfo[2] : "";
        $info = "type={$type}; code={$code}";
        $errorLog  = "Date : {$today} \n";
        $errorLog .= "Type : PDO \n";
        $errorLog .= "Title: {$errTitle} \n";
        $errorLog .= "Place: {$errPlace} \n";
        $errorLog .= "Info : {$info} \n";
        $errorLog .= "Message: {$message} \n \n";

        return $errorLog;
    }

}