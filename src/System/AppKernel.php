<?php

namespace Dzion\System;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Loader\YamlFileLoader;
use Symfony\Component\Routing\Router;

use Symfony\Component\HttpKernel\Controller\ControllerResolver;
use Symfony\Component\HttpKernel\Controller\ArgumentResolver;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Dotenv\Dotenv;


class AppKernel {

    protected $routes;
    protected $request;
    protected $response;
    protected $requestContext;
    protected $dispatcher;
    protected $rootPath;
    protected $pathInfo;

    public function __construct(string $rootPath, string $routesPath) {

        $this->rootPath = $rootPath;
        $envFilePath = $rootPath .'/config/.env';
        $this->setEnvParameters($envFilePath);
        $this->init($rootPath, $routesPath);
    }

    // Инициализация приложения
    protected function init(string $rootPath, string $routesPath):void {

        $this->request  = Request::createFromGlobals();  // Формируем Request
        $this->pathInfo = $this->request->getPathInfo(); // Получаем path_info

        // Формируем Request Context
        $this->requestContext  = new RequestContext();
        $this->requestContext->fromRequest($this->request);

        // Получаем и формируем routes
        $this->routes = $this->setRoutes($rootPath, $routesPath);

        $this->dispatcher = new EventDispatcher();

    }

    // Запускаем выплонение
    public function handle() : Response {

        $request = $this->request;
        $controllerResolver = new ControllerResolver();
        $argumentResolver = new ArgumentResolver();

        try {

            // Формируем активный контроллер
            $this->routesMatcher($this->routes, $this->requestContext, $this->pathInfo);

            // Запускаем контроллер на выполнение
            $controller = $controllerResolver->getController($request);
            $arguments = $argumentResolver->getArguments($request, $controller);
            $response = call_user_func_array($controller, $arguments);

        } catch (ResourceNotFoundException $exception) {
            $errorMessage = $exception->getMessage();
            $responseMessage = 'Не найдена страница : "' . $this->pathInfo . '"';
            $response = new Response($responseMessage, 404);

        } catch (Exception $exception) {

            $errorMessage = $exception->getMessage();
            $responseMessage = 'Произошла ошибка на сервере : "' . $errorMessage . '"';
            $response = new Response($responseMessage, 500);

        }

        return $response;
    }

    protected function setRoutes(string $rootPath, string $routesPath) : RouteCollection {
        $fileLocator = new FileLocator(array($rootPath));
        $yamlLoader = new YamlFileLoader($fileLocator);
        $options = ['cache_dir' => $rootPath . '/cache'];
        $router = new Router($yamlLoader, $routesPath, $options);
        $routes = $router->getRouteCollection();
        return $routes;
    }

    protected function routesMatcher($routesCollection, $requestContext) {
        $pathInfo = $this->request->getPathInfo();
        $matcher = new UrlMatcher($routesCollection, $requestContext);
        $attributes = $matcher->match($pathInfo);
        $this->request->attributes->add($attributes);
    }

    public function on($event, $callback){
        $this->dispatcher->addListener($event, $callback);
    }

    public function fire($event){
        return $this->dispatcher->dispatch($event);
    }

    public function setEnvParameters($envFilePath){
        $dotenv = new Dotenv();
        $dotenv->load($envFilePath);
    }

}