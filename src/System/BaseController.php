<?php

namespace Dzion\System;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Loader\YamlFileLoader;
use Symfony\Component\Config\FileLocator;
use Dzion\System\AppContainer;

abstract class BaseController
{
    protected $response;
    protected $container;
    protected $rootPath;
    protected $RESPONSE_NAME = 'result';

    public function __construct(){
        $this->rootPath = ROOT_PATH;
        $this->response = new JsonResponse();
        $this->container = new AppContainer();
    }

    protected function dataResponse(string $content){
        $this->response->setContent($content);
        return $this->response;
    }

    protected function jsonResponse($data , $additinalData = []){
        $response = [$this->RESPONSE_NAME => $data];
        if(!empty($additinalData)) {
            foreach ($additinalData as $fieldName => $item) {
                $response[$fieldName] = $item;
            }
        }
        $this->response->setData($response);
        return $this->response;
    }

    protected function controllerReflect($controller, $action, $request) {
        $refClass = new \ReflectionClass($controller);
        $parameters = $refClass->getMethod($action)->getParameters();
        $params = array();
        foreach($parameters as $param) {
            $name =  $param->name;
            if(!empty($attributes[$name])) {
                $params[] = $attributes[$name];
            }
        }

        $class = new $controller($request);
        $response = $class->$action(...$params);
        return $response;
  }

  protected function loadYamlFile($filePath, $rootPath){
        $fileLocator = new FileLocator(array($rootPath));
        $loader = new YamlFileLoader($fileLocator);
        $file = $loader->load($filePath);
        return $file;
  }

  public function getObject(string $key) {
        return $this->container->get($key);
  }

  protected function retriveJsonData() {
      $data = (array)json_decode(file_get_contents('php://input'));
      return $data;
  }

}